Rscript ../../scripts/proteome_uniquer.R -p "Homo_sapiens.GRCh38.pep.all.fa" -d "Homo_sapiens.GRCh38.pep.deduplicated.fa" -d "duplicates_of_transcript_ids.tsv"

Rscript ../../scripts/exploratory/protein_vs_tissue.R -p ../06_peptides_per_protein/kim/data -o . -d "kim"
Rscript ../../scripts/exploratory/protein_vs_tissue.R -p ../06_peptides_per_protein/wilhelm/data -o . -d "wilhelm"

Rscript ../../scripts/count_transcripts_per_gene.R -p ../../data/human_proteome/Homo_sapiens.GRCh38.pep.deduplicated.fa -o .

Rscript ../../../scripts/simultate_peptides/generate_peptide_distribution.R -m ../../../data/id_mapping/mapping_to_gene.tsv -e ../../06_peptides_per_protein/kim/data/colon_evidence_peps_per_gene.tsv -o .
