coveragePlotInput <- function(id, label = "CSV file") {
    # Create a namespace function using the provided id
    ns <- NS(id)
    
    tagList(
        fileInput(ns("file"), label),
        selectInput(ns('gene'), 'Options', gene.name, selectize=FALSE),
        selectInput(ns('tx1'), 'Options', tx1.name, selectize=FALSE),
        selectInput(ns('tx2'), 'Options', tx2.name, selectize=FALSE)
    )
}
coveragePlot <- function(input, output, session, stringsAsFactors) {
    observe({
        x <- input$file
        
        # Can use character(0) to remove all choices
        if (is.null(x))
            x <- character(0)
        
        # Can also set the label and select items
        updateSelectInput(session)
    })
}