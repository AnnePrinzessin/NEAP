
#mkdir results_uniprot_2015_final/10_major_minor_isoform/lamond/evidence
#mv -f results_uniprot_2015_final/10_major_minor_isoform/lamond/Lamond* results_uniprot_2015_final/10_major_minor_isoform/lamond/evidence
#mkdir results_ensembl_final/10_major_minor_isoform/rerun/rerun
#mkdir results_ensembl_final/10_major_minor_isoform/rerun/bRP
mkdir evaluation
mkdir evaluation/general
Rscript $1/scripts/splice_evaluation/major_minor_dists.R -i "results_uniprot_2015_final/10_major_minor_isoform/wilhelm:results_uniprot_2015_final/10_major_minor_isoform/kim:results_uniprot_2015_final/10_major_minor_isoform/lamond:results_ensembl_final/10_major_minor_isoform/rerun:results_uniprot_Mascot/10_major_minor_isoform/mascot" -p "longest" -n "all_longest" -o "evaluation/general"
Rscript $1/scripts/splice_evaluation/major_minor_dists.R -i "results_uniprot_2015_final/10_major_minor_isoform/wilhelm:results_uniprot_2015_final/10_major_minor_isoform/kim:results_uniprot_2015_final/10_major_minor_isoform/lamond:results_ensembl_final/10_major_minor_isoform/rerun:results_uniprot_Mascot/10_major_minor_isoform/mascot" -p "greedy" -n "all_greedy" -o "evaluation/general"
