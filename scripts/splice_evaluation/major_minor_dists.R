require(data.table)
require(ggplot2)
require(optparse)
require(gridExtra)
require(RColorBrewer)

option_list <- list(
    make_option(c("-i", "--input_folders"), type="character",
                help="Input Folder Splice files"),
    make_option(c("-p", "--pattern"), type="character",
                help="Name first dataset"),
    make_option(c("-n", "--name"), type="character",
                help="Name output file"),
    make_option(c("-o", "--outdir"), type="character",
                help="output folder")

)
opt <- parse_args(OptionParser(option_list=option_list))
#opt$input_folders<-"/home/proj/biocluster/praktikum/neap_pearl/hartebrodt_lehner/project_data/results_uniprot_2015_final/10_major_minor_isoform/kim:/home/proj/biocluster/praktikum/neap_pearl/hartebrodt_lehner/project_data/results_ensembl_final/10_major_minor_isoform/rerun"
inputs<-unlist(strsplit(opt$input_folders, ":"))
#opt$pattern ="longest"
print(inputs[1])
print(inputs[2])
maj_scores<-list()
min_scores<-list()
for (i in inputs){
  #print(i)
    dirs<-list.dirs(i, recursive = F)
    #print(dirs)
    for (d in dirs){
        l<-list.files(d)[grep(list.files(d), pattern=opt$pattern)]
        #print(file.path(d,list.files(d)[grep(list.files(d), pattern=opt$pattern)]))
        df<-tryCatch(
        fread(file.path(d,list.files(d)[grep(list.files(d), pattern=opt$pattern)])), 
        error=function(cond){
          return(NA)
        })
        if(is.na(df)){
          next
        }
        dd<-as.data.table(as.numeric(unlist(strsplit(df$"Score(s)_major_peptides", ";|\\|"))))
        dd$dataset<-l
        maj_scores[[d]]<-dd
        dd<-as.data.table(as.numeric(unlist(strsplit(df$"Score(s)_NOT_major_peptides", ";|\\|"))))
        dd$dataset<-l
        min_scores[[d]]<-dd
        }
}

M_s<-rbindlist(maj_scores)
m_s<-rbindlist(min_scores)
g_maj<-ggplot(M_s, aes(x=dataset, y=V1))+geom_boxplot(outlier.shape=NA)+ylim(0,250)+
  theme(axis.text.x = element_text(size=15),axis.text.y = element_text(size=5))+coord_flip()+
    ggtitle("Score distibution for peptides on major isoform")+
    xlab("Dataset")+ylab("Score")
g_min<-ggplot(m_s, aes(x=dataset, y=V1))+geom_boxplot(outlier.shape=NA)+ylim(0,250)+
  theme(axis.text.x = element_text(size=15),axis.text.y = element_text(size=5))+coord_flip()+
    ggtitle("Score distibution for peptides on minor isoforms")+
    xlab("Dataset")+ylab("Score")
png(file.path(opt$outdir, paste0(opt$name, "_scores_major.png")), width=600)
g_maj
dev.off()
png(file.path(opt$outdir, paste0(opt$name, "_scores_minor.png")), width=600)
g_min
dev.off()

maj_scores<-list()
min_scores<-list()
for (i in inputs){
    dirs<-list.dirs(i, recursive = F)
    for (d in dirs){
        l<-list.files(d)[grep(list.files(d), pattern=opt$pattern)]
        #print(l)
        print(file.path(d,list.files(d)[grep(list.files(d), pattern=opt$pattern)]))
        df<-tryCatch(
          fread(file.path(d,list.files(d)[grep(list.files(d), pattern=opt$pattern)])), 
          error=function(cond){
            return(NA)
          })
        if(is.na(df)){
          next
        }
        dd<-as.data.table(df$Number_of_peptides_ON_major_isoform)
        dd$dataset<-l
        maj_scores[[l]]<-dd
        dd<-as.data.table(df[Number_of_peptides_NOT_ON_major_isoform!=0]$Number_of_peptides_NOT_ON_major_isoform)
        dd$dataset<-l
        min_scores[[l]]<-dd
    }
}
M_s<-rbindlist(maj_scores)
m_s<-rbindlist(min_scores)
g_maj<-ggplot(M_s, aes(x=dataset, y=V1))+geom_boxplot(outlier.shape=NA)+ylim(0,25)+
  theme(axis.text.x = element_text(size=15),axis.text.y = element_text(size=5))+coord_flip()+
    ggtitle("Peptides on major isoform")+xlab("Dataset")+ylab("#Peptides")
png(file.path(opt$outdir, paste0(opt$name, "_peptides_major.png")), width=600)
g_maj
dev.off()
png(file.path(opt$outdir, paste0(opt$name, "_peptides_minor.png")), width=600)
g_min<-ggplot(m_s, aes(x=dataset, y=V1))+geom_boxplot()+
    theme(axis.text.x = element_text(size=15),axis.text.y = element_text(size=5))+coord_flip()+
    ggtitle("Peptides on minor isoform")+xlab("Dataset")+ylab("#Peptides")
g_min
dev.off()

number_min_iso<-list()
for (i in inputs){
  print(i)
  dirs<-list.dirs(i, recursive = F)
  print(dirs)
  for (d in dirs){
    l<-list.files(d)[grep(list.files(d), pattern=opt$pattern)]
    print(l)
    #print(file.path(d,list.files(d)[grep(list.files(d), pattern=opt$pattern)]))
    df<-tryCatch({
      #print(file.path(d,list.files(d)[grep(list.files(d), pattern=opt$pattern)]))
      fread(file.path(d,list.files(d)[grep(list.files(d), pattern=opt$pattern)]))}, 
      error=function(cond){
        return(NA)
      })
    if(is.na(df)){
      next
    }
    min<-df[Number_of_peptides_NOT_ON_major_isoform!=0, .N]
    all<-df[, .N]
    number_min_iso[[l]]<-list(l, all, min)
  }
}

df<-as.data.table(do.call(rbind, number_min_iso))
colnames(df)<-c("Dataset", "Total", "Minor")
fwrite(df, file.path(opt$outdir, paste0(opt$name, "table_major_minor.tsv")), sep="\t")
df<-df[,c("Dataset", "Minor")]
df<-melt(df, id.vars="Dataset")
pal<-c("#0033cc", "#668cff")
g<-ggplot(df, aes(x=unlist(Dataset),y=unlist(value)))+
  geom_bar(stat= "identity")+
xlab("Dataset")+theme(axis.text.y=element_text(size=5), axis.text.x=element_text(size=15))+
  ylab("#Number of Isoforms")+
  coord_flip()+
  ggtitle("Number of Minor Isoforms")
png(file.path(opt$outdir, paste0(opt$name, "_number_minor_isoforms.png")), width=600)
plot(g)
dev.off()


