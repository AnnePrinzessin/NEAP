require(data.table)
require(ggplot2)
require(optparse)
require(gridExtra)

option_list <- list(
    make_option(c("-f", "--first"), type="character",
                help="Name first dataset"),
    make_option(c("-s", "--second"), type="character",
                help="Name second dataset"),
    make_option(c("-i", "--general"), type="character",
                help=" General Stats"),
    make_option(c("-t", "--splice"), type="character",
                help="Splice overlap"),
    make_option(c("-n", "--name"), type="character",
                help="Outdir"),
    make_option(c("-o", "--outdir"), type="character",
                help="Outdir")
)
opt <- parse_args(OptionParser(option_list=option_list))

#opt$first<-"Wilhelm"
#opt$second<-"Kim"
#opt$splice<-"/home/proj/biocluster/praktikum/neap_pearl/hartebrodt_lehner/project_data/evaluation/wilhelm_kim_same_tissue_greedy_MQ_uniprot/wilhelm_kim_same_tissue_greedy_MQ_uniprot_splice.tsv"
#opt$general<-"/home/proj/biocluster/praktikum/neap_pearl/hartebrodt_lehner/project_data/evaluation/kim_ensemble_kim_uniprot_equal_MQ/kim_ensemble_kim_uniprot_equal_MQ_stats.tsv"
#opt$name
#opt$outdir
splice<-fread(opt$splice)
colnames(splice)<-c("Dataset1", "Dataset2", "Proteins_1", "Proteins_2", "Intersect", "Same_Gene", "Same_Gene_and_Isoform", "Total")
#splice<-splice[-which(Dataset1==Dataset2),]
splice_melt<-melt(splice, measure.vars = c("Proteins_1", "Proteins_2", "Intersect", "Same_Gene", "Same_Gene_and_Isoform", "Total"))
g<-ggplot(splice_melt[variable %in% c("Proteins_1", "Proteins_2", "Intersect")], aes(y=value, x=factor(variable)))+geom_boxplot()+
    scale_x_discrete(labels=c(opt$first, opt$second , "Intersect"))+xlab("Dataset")+
    ylab("#Identified Proteins")+ggtitle("Number of Identfied Proteins in Datasets")+theme(axis.text=element_text(size=15))
h<-ggplot(splice_melt[variable %in% c("Total", "Same_Gene_and_Isoform")], aes(x=factor(variable, levels=c( "Total", "Same_Gene_and_Isoform")), y=value))+
    scale_x_discrete(labels=c("Same Gene", "Same Major Isoform"))+
    ggtitle("Average Size of Overlap of Genes with Alternative Isoforms")+
    ylab("#Genes or Isoforms")+xlab("")+geom_boxplot()+geom_point()+theme(axis.text=element_text(size=15))
png(file.path(opt$outdir, paste0(opt$name, "_overlap_genes_iso.png")), width=600)
plot(h)
dev.off()
png(file.path(opt$outdir, paste0(opt$name, "_identified_proteins.png")), width=600)
plot(g)
dev.off()

general<-fread(opt$general)
general<-unique(general)
names(general)<-c("NrProteins","dataset", "major_peps", "minor_peps", "fraction")
general$Approach<-rep("longest",length(general$dataset))
general$Approach[grep("greedy", general$dataset)]<-"greedy"
k2<-ggplot(general, aes(x=dataset, y= fraction, fill=Approach))+geom_bar(stat="identity")+
    theme(legend.position="bottom",legend.key.width=unit(0.3,"cm"),legend.key.height=unit(0.3,"cm"),axis.text.x= element_text(size=15), axis.text.y=element_text(size=5))+xlab("Dataset")+
    ylab("Fraction of minor peptides")+ggtitle("Fraction of peptides mapping to minor isoform")+coord_flip()+scale_y_continuous(labels = scales::percent)
png(file.path(opt$outdir, paste0(opt$name, "_fraction_minor_peptides.png")), width=600)
k2
dev.off()

