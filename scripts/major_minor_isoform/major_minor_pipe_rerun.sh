#!/bin/bash

# script to count peptide matches on major or minor isoform for results/02_mapping_to_evidences/trypsin_l50_m2/kim and results/02_mapping_to_evidences/trypsin_l50_m2/wilhelm
# two different strategies: -t / -g
# usage example: "bash major_minor.sh <my_output_file_name(no file ending)> -t/-g -h <proteome_file.tsv(only with -t)>"

i_end=''
data=""
output=$2
approach=$3
header=$4
evidence=$5
lab=$6
if [[ "$#" -ne 7 ]];
then
proteome=$7
scriptfolder=$8
else
scriptfolder=$7
fi
#echo $scriptfolder

for i in $1/* ; do 	#kim data set
        if [[ $i =~ "rerun" ]]; then

                i_end=${i##*/}          # file for -f
                i_end_cov=$i_end"_coverage.tsv"
                i_end_folder=${1##*/}
                data=$i_end_folder
                i_end_txt=$i_end".txt"           # file for -e
                proteome_name=${proteome##*/}
                proteome_name_no_tsv=${proteome_name%%.tsv}
                i_end_plus_approach=$i_end"_"$output"_"$proteome_name_no_tsv".tsv"	#concat strings: pancreas -> pancreas_$output_$proteome

                mkdir -p ./10_major_minor_isoform/$lab
                echo "./10_major_minor_isoform/$lab/$i_end_plus_approach"

                perl $scriptfolder/scripts/major_minor_isoform/major_minor_isoform.pl -d 45 -f $1/$i_end/$i_end_cov -e $evidence/$i_end_txt -o /home/nick/local/biocluster/praktikum/neap_pearl_ss17/hartebrodt_lehner/project_data/move_me_to_results_ensembl_final/10_major_minor_isoform/$lab/$i_end_plus_approach $approach $header -p $proteome
        #        perl $scriptfolder/scripts/major_minor_isoform/major_minor_isoform.pl -d 45 -f $1/$i_end/$i_end_cov -e $evidence/$i_end_txt -o ./10_major_minor_isoform/$lab/$i_end_plus_approach $approach $header -p $proteome
        fi
done

echo "========== Data set $data $approach finished =========="
