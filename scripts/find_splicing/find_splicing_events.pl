# @autor: nick
# Uses mapping (and transcriptome) file to identify splicing event.
# Two mutually exclusive options:
# 1. Broad / naive identification (parameter flag -b)
# 2. Tress et al. identification (parameter flag -t)

use Bio::Tools::dpAlign;
use Bio::SeqIO;
use Bio::SimpleAlign;
use Bio::AlignIO;
use Bio::Matrix::IO;
use Getopt::Std;
#use Align_it::NW;
use Data::Dumper;

# test for the existence of the options on the command line
if(@ARGV==0){
die("Parameters to be used:
-b/-t\t-b for broad/naive identification; or -t for Tress et al. identification
-f\tpath to mapping file
-p\tpath to transcriptome file
-o\tpath to output file\n")}

# declare the perl command line flags/options we want to allow
my %options=();
getopts("btf:p:o:", \%options);

### parameter checking & open files
print "Parameters used:\n";

if (defined $options{t} && defined $options{b}) {
	die("ERROR: Specify only one approach!\n-b for broad/naive identification\n-t for Tress et al. identification\n");
}

if(defined $options{t}){		# parameter flag checking
	print "-t option used (Tress et al. approach)\n";
	if(defined $options{p}){
		print "-p $options{p}\n";
		open(IN_prot, '<', $options{p}) or die("\nNo proteome file found! Use relative path.\n"); #mapping file open
	}
	else{
		die("ERROR: Missing/incomplete parameter -p (must contain path to transcriptome file)\n");
	}
	if(defined $options{f}){
		print "-f $options{f}\n";
		open(IN_M, '<', $options{f}) or die("\nNo mapping file found! Use relative path.\n"); #mapping file open
	}
	else{
		die("ERROR: Missing/incomplete parameter -f (must contain path to mapping file)\n");
	}
	if(defined $options{o}){
		print "-o $options{o}\n";
		open(OUT, '>', $options{o});	#output file
	}
	else{
		die("ERROR: Missing/incomplete parameter -o (must contain path to output file)\n");
	}
}
elsif(defined $options{b}){
	print "-b option used (naive approach)\n";
	if(defined $options{f}){
		print "-f $options{f}\n";
		open(IN_M,	'<', $options{f}) or die("\nNo mapping file found! Use relative path.\n"); #mapping file open
	}
	else{
		die("ERROR: Missing/incomplete parameter -f (must contain path to mapping file)\n");
	}
	if(defined $options{o}){
		print "-o $options{o}\n";
		open(OUT,	'>', $options{o});	#output file
	}
	else{
		die("ERROR: Missing/incomplete parameter -o (must contain path to output file)\n");
	}
}
else{
	die("ERROR: Missing parameter -b OR -t\n");
}
###


$count=0;
%mapping=();
while(<IN_M>){		#load mapping file and save into hash %mapping
	chomp($_);
	my $line=$_;
	$count++;
	if ($count>1){	#skip header
		my @split_line=split("\t",$line);

		my $peptide=$split_line[0];
		my $gene_name = $split_line[1];	
		my $t1=$split_line[2];	#transcript 1
		my $t2=$split_line[3];	#transcript 2
		shift @split_line for 1..4;
		my $evidence = join("\t",@split_line);

		$mapping{$gene_name}{$t1}{$t2}{$peptide}{$evidence}=1;
	}
	else{	#save header
		$header = $line;
	}
}
close IN_M;
print "Mapping file loaded.\n";
#print Dumper %mapping;

### identify splice events
if (defined $options{b}){	# Naive/broad
	print OUT "$header\n";
	foreach my $gene_name (keys %mapping){
	#	print "$gene_name\n";
		foreach my $transcript1 (keys %{$mapping{$gene_name}}){
		#	print "\t$transcript1\n";
			foreach my $transcript2 (keys %{$mapping{$gene_name}{$transcript1}}){
		#		print "\t\t$transcript2\n";
				foreach my $peptide (keys %{$mapping{$gene_name}{$transcript1}{$transcript2}}){
				#	print "\t\t\t$peptide\n";
					foreach my $evidence (keys %{$mapping{$gene_name}{$transcript1}{$transcript2}{$peptide}}){
						#print "\t\t\t\t$evidence\n";
						if (exists $mapping{$gene_name}{$transcript2}{$transcript1}){	#switched t1-t2 -> t2-t1
							print OUT "$peptide\t$gene_name\t$transcript1\t$transcript2\t$evidence\n"
						}
					}
				}
			}
		}
	}
}
else {	#Tress et al.
	print "Tress approach\n";

	$count=0;
	%mapping_prot=();
	while(<IN_prot>){		#load proteome file and save into hash %mapping_prot
		chomp($_);
		my $line=$_;
		$count++;
		if ($count>1){	#skip header
			my @split_line=split("\t",$line);

			my $protein_sequence=$split_line[2];
			my $gene_name = $split_line[0];	
			my $t1=$split_line[1];	#transcript 1

			$mapping_prot{$gene_name}{$t1}{$protein_sequence}=1;
		}
	}


	### identify splice events
	print OUT "$header\n";
	foreach my $gene_name (keys %mapping){
	#	print "$gene_name\n";
		foreach my $transcript1 (keys %{$mapping{$gene_name}}){
		#	print "\t$transcript1\n";
			foreach my $transcript2 (keys %{$mapping{$gene_name}{$transcript1}}){
		#		print "\t\t$transcript2\n";
				foreach my $peptide (keys %{$mapping{$gene_name}{$transcript1}{$transcript2}}){
				#	print "\t\t\t$peptide\n";
					foreach my $evidence (keys %{$mapping{$gene_name}{$transcript1}{$transcript2}{$peptide}}){
						#print "\t\t\t\t$evidence\n";
						align $mapping_prot{$gene_name}{$transcript1} to $mapping_prot{$gene_name}{$transcript2}
						if (exists $mapping{$gene_name}{$transcript2}{$transcript1}){	#switched t1-t2 -> t2-t1
							print OUT "$peptide\t$gene_name\t$transcript1\t$transcript2\t$evidence\n"
						}
					}
				}
			}
		}
	}

   	print "$result\n";
}
print "\n";



# sub read_fasta_sequence {		# fasta reader from "http://code.izzid.com/2011/10/31/How-to-read-a-fasta-file-in-perl.html"
#    my ($fh_proteome, $seq_info) = @_;

#    $seq_info->{seq} = undef; # clear out previous sequence

#    # put the header into place
#    $seq_info->{header} = $seq_info->{next_header} if $seq_info->{next_header};

#    my $file_not_empty = 0; 
#    while (<$fh_proteome>) {
#       $file_not_empty = 1;
#       next if /^\s*$/;  # skip blank lines
#       chomp;    

#       if (/^>/) { # fasta header line
#          my $h = $_;    
#          $h =~ s/^>//;  
#          if ($seq_info->{header}) {
#             $seq_info->{next_header} = $h;
#             return $seq_info;   
#          }              
#          else { # first time through only
#             $seq_info->{header} = $h;
#          }              
#       }         
#       else {    
#          s/\s+//;  # remove any white space
#          $seq_info->{seq} .= $_;
#       }         
#    }    

#    if ($file_not_empty) {
#       return $seq_info;
#    }    
#    else {
#       # clean everything up
#       $seq_info->{header} = $seq_info->{seq} = $seq_info->{next_header} = undef;
#       return;   
#    }    
# }