#!/bin/bash

# script to find splicing events for every mapping file in results/02_mapping_to_evidences/trypsin_l50_m2/kim and results/02_mapping_to_evidences/trypsin_l50_m2/wilhelm
# usage example: "bash splicing_events.sh -b splicing_events.txt"

i_end=''
flag=$1
output=$2

for i in ../../results/02_mapping_to_evidences/trypsin_l50_m2/kim/* ; do 	#kim data set

        i_end=${i##*/}          #cut for example ../results/02_mapping_to_evidences/trypsin_l50_m2/kim/pancreas -> pancreas
        mkdir -p ../../results/08_identify_splicing_events/kim/$i_end
        perl find_splicing_events.pl $flag -f $i/transcript_gene_disc_mapping.tsv -o ../../results/08_identify_splicing_events/kim/$i_end/$output

done

echo "========== Kim data set finished =========="

for i in ../../results/02_mapping_to_evidences/trypsin_l50_m2/wilhelm/* ; do   #kim data set

        i_end=${i##*/}          #cut for example ../results/02_mapping_to_evidences/trypsin_l50_m2/wilhelm/pancreas -> pancreas
        mkdir -p ../../results/08_identify_splicing_events/wilhelm/$i_end
        perl find_splicing_events.pl $flag -f $i/transcript_gene_disc_mapping.tsv -o ../../results/08_identify_splicing_events/kim/$i_end/$output

done

echo "========== Wilhelm data set finished =========="