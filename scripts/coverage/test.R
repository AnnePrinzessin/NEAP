require(rtracklayer)
require(data.table)
require(stringr)
require(Biostrings)
require(Pviz)

source("~/NEAP/NEAP/scripts/app_new/color_scheme.R")
gtf<-import("/home/anne/NEAP/NEAP/data/genome_annotation/gencode.v26.annotation.gtf.gz.1", format = "gtf")
protein_coding<-gtf[gtf@elementMetadata$type=="CDS" & gtf@elementMetadata$gene_type=="protein_coding", c("gene_id", "gene_name", "transcript_id", "protein_id")]
#export(protein_coding, "/home/anne/NEAP/NEAP/data/genome_annotation/gencode.v26.annotation.protein_coding.gtf.gz", format = "gtf")
id_mapping<-fread("~/NEAP/NEAP/data/id_mapping/uniprot2015_to_ENST.tab", header=T, sep="\t")
du<-which(duplicated(id_mapping$To))
id_mapping<-id_mapping[-du]
transcript_ids<-sapply(protein_coding@elementMetadata$transcript_id, function(x) gsub("\\.[0-9]*", "", x))
#transcript_ids<-unique(transcript_ids)
dat<-data.table(tx_id=transcript_ids, tx_full=protein_coding@elementMetadata$transcript_id)
mm<-merge(dat, id_mapping, by.x="tx_id", by.y="To")
protein_coding@elementMetadata$protein_name<-"1"
protein_coding[transcript_ids %in% mm$tx_id]@elementMetadata$protein_name<-mm[order(match(mm$tx_id, transcript_ids))]$From

#works
#ENSG00000158467.16
#ENST00000446544.6
#ENST00000490911.5

#not
#ENSG00000104231.10
#ENST00000523096.5
#ENST00000522520.5
#not
#ENSG00000188610.12
#ENST00000369390.7
#ENST00000619376.4

#ENST00000535872.1
#ENST00000535107.5

test1<-protein_coding[protein_coding@elementMetadata$transcript_id=="ENST00000535872.1"]
test2<-protein_coding[protein_coding@elementMetadata$transcript_id=="ENST00000535107.5"]
proteome<-fread("/home/anne/NEAP/NEAP/data/human_proteome/Homo_sapiens.GRCh38.pep.all.deduplicated.tsv", header=T, sep="\t")

seq1<-proteome[transcript=="ENST00000535872.1"]
seq2<-proteome[transcript=="ENST00000535107.5"]

#rebase Granges
mi<-min(min(test1@ranges@start),min(test2@ranges@start))
ma<-max(max(test1@ranges@start+test1@ranges@width[length(test1@ranges)]),max(test2@ranges@start+test2@ranges@width[length(test2@ranges)]))
test1@ranges@start<-test1@ranges@start-mi
test1@ranges@start<-test1@ranges@start+as.integer(1)
#test1@ranges@width<-as.integer(round(test1@ranges@width/3))

test2@ranges@start<-test2@ranges@start-mi
test2@ranges@start<-test2@ranges@start+as.integer(1)
#test2@ranges@width<-as.integer(round(test2@ranges@width/3))
v1<-rep(0, (ma-mi))
v2<-rep(0, (ma-mi))

r1<-as.data.table(test1@ranges)
r2<-as.data.table(test2@ranges)
for (l in 1:nrow(r1)){
    print(l)
    v1[r1[l,start]:r1[l, end]]<-1   
}
for (l in 1:nrow(r2)){
    print(l)
    v2[r2[l,start]:r2[l, end]]<-1   
}

w<-sapply(1:length(v1), function(x) ifelse(v1[x]==0&&v2[x]==0, x, 0))
w<-w[w!=0]

v1<-v1[-w]
v2<-v2[-w]
if(test1@strand@values=="-"){
    v1<-rev(v1)
    v2<-rev(v2)
}

merger<-function(x1,x2,x3){
    if(is.na(x2) || is.na((x3))){
        return(x1)
    }
    if(x1==x2 && x2==x3){
        return(x1)
    }
    else{
        if(x1==1){
            return(1)
        }
        else{
            return(1)
        }
    }
}
se<-seq(1, length(v1), 3)
vv1<-sapply(se, function(x) merger(v1[x],v1[x+1], v1[x+2]))
vv2<-sapply(se, function(x) merger(v2[x],v2[x+1], v2[x+2]))




aligner<-function(seq, vv){
    cur_ind=1
    s<-c()
    for (i in 1:length(vv)){
        if(vv[i]==1){
            s<-c(s, str_sub(seq$protein, cur_ind, cur_ind))
            cur_ind=cur_ind+1
        }
        else{
            s<-c(s, "-")
        }
    }
    return(AAString(paste0(s, collapse = "")))
}
s1<-aligner(seq1, vv1)
s2<-aligner(seq2, vv2)
p1<-ProteinSequenceTrack(s1)
p2<-ProteinSequenceTrack(s2)
plotTracks(list(p1,p2), from=1, to=length(s1), fontcolor=fcol)

highlighter<-function(s2, test2){
    l<-str_locate_all(as.character(s2), "-*")
l<-as.data.table(l)
l<-l[start<end]
r<-test2@ranges@width/3
for (i in 1:length(r)){
    r2[i]<-sum(r[1:i])
}
r<-r2
for(i in 1:nrow(l)){
    idx<-which(r>l$start[i])
r[idx]<-r[idx]+l$end[i]-l$start[i]+1
}
ra<-r
r<-c(1,r)
hl2<-HighlightTrack(p2, start=r, end=r)
return(hl2)
}
coverage<-fread("~/NEAP/project/adrenal.gland_evidence_coverage_unique.tsv", header=T, sep="\t")
cov1<-coverage[tx=="ENST00000535872.1"]
cov2<-coverage[tx=="ENST00000535107.5"]
peptideAligner<-function(s2, cov2){
l<-str_locate_all(as.character(s2), "-*")
l<-as.data.table(l)
l<-l[start<end]
for(i in 1:nrow(l)){
    idx<-which(l[i, start]<cov2$start)
    cov2[idx, 3]<-cov2[idx, 3]+l[i, end]-l[i, start]+1
    cov2[idx, 4]<-cov2[idx, 4]+l[i, end]-l[i, start]+1
    aa<-cov2$start>l[i, start]
    bb<- cov2$stop<l[i, end]
    ind<-which(aa == bb)
    for( idx in ind){
        seq<-str_split(cov2$pep[idx], "")[[1]]
    a<-paste0(seq[1:(l[i,start]-cov2$start[idx])], collapse="")
    b<-paste0(rep("-", (l[i, end]-l[i, start]+1)), collapse = "")
    c<-paste0(seq[(l[i,start]-cov2$start[idx]+1):length(seq)] , collapse = "")
    cov2[idx, 5]<-paste0(a,b,c, collapse = "")
    cov2[idx, 4]<-cov2[idx, 4]+l[i, end]-l[i, start]+1
    }
}
colna<-colnames(cov2)
cov2<-cov2[, .N, by=colna]
cov2$start<-as.numeric(cov2$start)
cov2$N<-as.numeric(cov2$N)
pal<-rev(viridis(max(cov2$N)))
hl<-ProbeTrack(sequence = unlist(cov2$pep), intensity = unlist(cov2$N), probeStart = unlist(cov2$start), cex=0, legend=T, color=pal, cex.legend=0.5)
hl
}
hl2<-peptideAligner(s2, cov2)
hl1<-peptideAligner(s1,cov1)
plotTracks(list(p1,p2, hl2), fontcolor=fcol)

