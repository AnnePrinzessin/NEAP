#!/bin/bash
#$1 root directory
#
mkdir simulation
cd simulation
mkdir distribution
Rscript $1/scripts/simulate_peptides/generate_peptide_distribution.R -m $1/data/id_mapping/mapping_to_gene.tsv -o distribution -e ../exploratory/peptides_per_protein/Kim_Search_Results/colon_evidence_peps_per_gene.tsv
for dir in $(ls ../01_in_silico_digest);
do
mkdir $dir
Rscript $1/scripts/simulate_peptides/adjust_peptide_length.R -p ../01_in_silico_digest/$dir/$dir\_peptides.tsv -t ../01_in_silico_digest/$dir/$dir\_peptides_6to50.tsv
declare -a ratio=(1 5 10 50 100)
seeds=$(seq 10)
for r in $ratio;
do
for s in $seeds;
do
Rscript $1/scripts/simulate_peptides/estimate_splicing_2.R -p $1/data/human_proteome/uniprot2015.deduplicated.tsv -t ../01_in_silico_digest/$dir/$dir\_peptides_6to50.tsv -o "$dir" -s $s -d distribution/peps_per_gene_dist.tsv -r $r
done
done
mkdir $dir/coverage_simulated
for file in $(ls $dir/*.tsv); do
  echo $file
  bn=$(basename $file .tsv)
  echo $bn
  python3 $1/scripts/coverage/coverage_aligned.py $file $dir/coverage_simulated $1/data/human_proteome/uniprot2015.fasta $bn"_coverage.tsv" "uniprot"
  Rscript $1/scripts/coverage/coverage_uni.R -e $dir/coverage_simulated/$bn\_coverage.tsv -o $dir/coverage_simulated/$bn"\_coverage_unique.tsv"
done
done
