mkdir data
cd data

#second argument: folder to be created
mkdir $2
cd $2

# first argument evidence file directory
for f in $1/*evidence.txt;
do
    bn=$(basename $f .txt)
    python3 ../../../../scripts/coverage/coverage_aligned.py $f $bn ../../../../data/human_proteome/Homo_sapiens.GRCh38.pep.all.fa $bn"_coverage.tsv"
    Rscript ../../../../scripts/coverage/coverage_uni.R -e $(pwd)/$bn/$bn"_coverage.tsv" -o $(pwd)/$bn/$bn"_coverage_unique.tsv"
done
